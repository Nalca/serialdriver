/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm/irq.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include "SerialDriver.h"

static int			Major;
static int			Device_Open = 0;
static wait_queue_head_t	wq_read;
static wait_queue_head_t	wq_write;

int speed = (int)VIT_9600;
int parite = (int)PARITE_NONE;
int bitstop = (int)STOP_1;
int datasize = (int)SIZE_8;

module_param(speed, int, S_IRUGO);
module_param(parite, int, S_IRUGO);
module_param(bitstop, int, S_IRUGO);
module_param(datasize, int, S_IRUGO);

static irqreturn_t serial_irq(int cpl, void *dev_id);
static int serial_open(struct inode *inode, struct file *file);
static int serial_close(struct inode *inode, struct file *file);
static ssize_t serial_read(struct file *filep, char *buffer, size_t length, loff_t *offset);
static ssize_t serial_write(struct file *filep, const char *buffer, size_t length, loff_t *offset);
static unsigned int serial_poll(struct file *filep, struct poll_table_struct *pollt);

static struct file_operations fops = {
  .owner = THIS_MODULE,
  .read = serial_read,
  .write = serial_write,
  .open = serial_open,
  .release = serial_close,
  .poll = serial_poll
};

int __init __init_module(void)
{
  printk("%s: Hello !\n", MODULE_NAME);
  /* La config de la chose (Selon les options choisies) */

  // Cours toujours pour le parsing de valeur
  if (CheckConfig())
    return (-1);
  if (ShowConfig())
    return (-1);

  // Couché les interruptions
  outb(0x00, UART_ITR_ENABLE); 
  // Active le mode fifo
  outb(0xC7, UART_FIFO_CONTROL);

  if (SetSerialSpeed(speed) == -1)
    {
      printk("%s: invalid speed.\n", MODULE_NAME);
      return (-1);
    }
  if (request_irq(UART_IRQ, &serial_irq, IRQF_SHARED, MODULE_NAME, &MODULE_NAME) != 0)
    {
      printk("%s: request_irq failed.\n", MODULE_NAME);
      return (-EBUSY);
    }

  // Configure taille, bit de stop et parité
  outb(datasize | (bitstop << 2) | (parite << 3), UART_LINE_CONTROL);
  // Options du modem
  outb(0x3, UART_MODEM_CONTROL);

  // Lew waits_queues pour read/write
  init_waitqueue_head(&wq_read);
  init_waitqueue_head(&wq_write);

  // Active les interruptions
  outb(0x0F, UART_ITR_ENABLE);
  outb( 0x0B, UART_MODEM_CONTROL);

  Major = register_chrdev(0, "serialdrv", &fops);
  if (Major > 0)
    {
      printk("%s: register_chrdev successfull - %d\n", MODULE_NAME, Major);
      return (0);
    }
  else
    {
      printk("%s: register_chrdev failed.\n", MODULE_NAME);
      return (-1);
    }
}

void __exit __exit_module(void)
{
  printk("%s: Bye !\n", MODULE_NAME);
  if (Major > 0)
    {
      free_irq(UART_IRQ, &MODULE_NAME);
      unregister_chrdev(Major, "serialdrv");
    }
  else
    printk("%s: THIS EXIT SHOULD NOT HAPPEN !\n", MODULE_NAME);
}

module_init(__init_module);
module_exit(__exit_module);


// OPEN -------------------------------------------------------------------------------------
static int serial_open(struct inode *inode, struct file *file)
{
  if (Device_Open) // Un seul à la fois, c'est déjà assez compliqué comme ça
    {
      printk("%s: Il ne peut n'y avoir qu'un seul !\n", MODULE_NAME);
      return (-1);
    }
  ++Device_Open;
  printk("%s: Device opened.\n", MODULE_NAME);
  return (0);
}

// CLOSE ------------------------------------------------------------------------------------
static int serial_close(struct inode *inode, struct file *file)
{
  if (Device_Open > 0)
    {
      --Device_Open;
      printk("%s: Device closed.\n", MODULE_NAME);
    }
  return (0);
}

// READ -------------------------------------------------------------------------------------
static ssize_t serial_read(struct file *filep, char *buffer, size_t length, loff_t *offset)
{
  int i;
  int count;

  if (UART_DATA_READY_INDICATOR == 0)
    {
      if (filep->f_flags & O_NONBLOCK)
	return (0);
      if (wait_event_interruptible(wq_read, UART_DATA_READY_INDICATOR))
	return (-EINTR);
    }

  i = 0;
  count = 0;
  while (i < length)
    {
      unsigned char data;
	  
      ++count;
      data = inb(UART_BUFFER);
      if (copy_to_user(buffer + i, &data, 1))
	return (-EFAULT);
      if (!UART_DATA_READY_INDICATOR)
	break;
      ++i;
    }
  return (count);
}

// WRITE ------------------------------------------------------------------------------------
static ssize_t serial_write(struct file *filep, const char *buffer, size_t length, loff_t *offset)
{
  int i;
  int count;

  if (!UART_CLEAR_TO_SEND)
    {
      if (filep->f_flags & O_NONBLOCK)
	return (0);
      if (wait_event_interruptible(wq_write, UART_CLEAR_TO_SEND))
	return (-EINTR);
    }


  i = 0;
  count = 0;
  while (i < length)
    {
      char data;
	  
      if (copy_from_user(&data, buffer + i, 1))
	return (-EFAULT);
      while ((UART_TRANSMITTER_HOLDING_EMPTY) == 0);
      outb(data, UART_BUFFER);
      ++count;
      if (!UART_CLEAR_TO_SEND)
	break;
      ++i;
    }
  return (count);
}

// POLL -------------------------------------------------------------------------------------
static unsigned int serial_poll(struct file *filep, struct poll_table_struct *pollt)
{
  unsigned int mask = 0;

  poll_wait(filep, &wq_read, pollt);
  poll_wait(filep, &wq_write, pollt);

  if (UART_DATA_READY_INDICATOR)
    mask |= (POLLIN | POLLRDNORM);
  if (UART_TRANSMITTER_HOLDING_EMPTY)
    mask |= (POLLOUT | POLLWRNORM);
  return (mask);
}


static irqreturn_t serial_irq(int cpl, void *dev_id)
{
  char interruptId = inb(UART_ITR_REGISTER);

  if (interruptId & 1) // Aucune interruption dans ce cas
    return (IRQ_NONE);
  
  interruptId = interruptId & 0x0E; // Seuls les bits 1, 2 et 3 ont un intérêt
  switch (interruptId)
    {
    case 0:
      inb(UART_LINE_STATUS);
      break;

    case 2:
      wake_up_interruptible(&wq_write);
      break;
      
    case 4:
      wake_up_interruptible(&wq_read);
      break;

    case 6:
      inb(UART_LINE_STATUS);
      break;

    case 12:
      wake_up_interruptible(&wq_read);
      break;

    default:
      printk("%s: THIS SHOULD NOT HAPPEN WITH INTERRUPT (%d)\n", MODULE_NAME, (int)(interruptId));
      return (-1);
    }
  return (IRQ_HANDLED);
}

int	SetSerialSpeed(SERIAL_SPEED speed)
{
  char dll;
  char dlm = 0;

  switch (speed)
    {
    case VIT_300:
      dll = 0x80;
      dlm = 0x01;
      break;
      
    case VIT_1200:
      dll = 0x60;
      break;
      
    case VIT_2400:
      dll = 0x30;
      break;
      
    case VIT_4800:
      dll = 0x18;
      break;
      
    case VIT_9600:
      dll = 0x0c;
      break;
      
    case VIT_19200:
      dll = 0x06;
      break;
      
    case VIT_38400:
      dll = 0x03;
      break;
      
    case VIT_57600:
      dll = 0x02;
      break;
      
    case VIT_115200:
      dll = 0x01;
      break;
      
    default:
      return (-1);
    }

  outb(0x80, UART_LINE_CONTROL);	// On active l'accès au divisor latch
  outb(dll, UART_DL_LS);		// On selectionne la bonne valeur de vitesse pour le port série
  outb(dlm, UART_DL_MS);
  outb(0, UART_LINE_CONTROL);		// Accès au divisor latch désactivé
  return (0);
}

int	CheckConfig(void)
{
  if (parite < PARITE_NONE || parite > PARITE_PAIRE)
    {
      printk("%s: invalid 'parite' value : %d\n", MODULE_NAME, parite);
      return (-1);
    }
  if (bitstop < STOP_1 || bitstop > STOP_1HALF)
    {
      printk("%s: invalid 'bitstop' value : %d\n", MODULE_NAME, bitstop);
      return (-1);
    }
  if (datasize < SIZE_5 || datasize > SIZE_8)
    {
      printk("%s: invalid 'datasize' value : %d\n", MODULE_NAME, datasize);
      return (-1);
    }
  if (speed < VIT_300 || speed > VIT_115200)
    {
      printk("%s: invalid 'speed' value : %d\n", MODULE_NAME, speed);
      return (-1);
    }
  return (0);
}

int	ShowConfig(void)
{
  const char *s_parite;
  const char *s_bitstop;
  const char *s_datasize;
  const char *s_speed;

  switch ((SERIAL_SPEED)(speed))
    {
    case VIT_300:
      s_speed = "300"; break;

    case VIT_1200:
      s_speed = "1200"; break;

    case VIT_2400:
      s_speed = "2400"; break;

    case VIT_4800:
      s_speed = "4800"; break;

    case VIT_9600:
      s_speed = "9600"; break;

    case VIT_19200:
      s_speed = "19200"; break;

    case VIT_38400:
      s_speed = "38400"; break;

    case VIT_57600:
      s_speed = "57600"; break;

    case VIT_115200:
      s_speed = "115200"; break;

    default:
      printk("%s: invalid 'speed' value : %d\n", MODULE_NAME, speed);
      return (-1);
    }

  switch ((SERIAL_PARITE)(parite))
    {
    case PARITE_NONE:
      s_parite = "none";
      break; 

    case PARITE_IMPAIRE:
      s_parite = "impaire";
      break; 

    case PARITE_NONE_2:
      s_parite = "none_2";
      break;

    case PARITE_PAIRE:
      s_parite = "paire";
      break;

    default:
      printk("%s: invalid 'parite' value : %d\n", MODULE_NAME, parite);
      return (-1);
    }

  switch ((SERIAL_BIT_STOP)(bitstop))
    {
    case STOP_1:
      s_bitstop = "1";
      break;

    case STOP_2:
      //    case STOP_1HALF: // Compile pas sinon
      s_bitstop = "2 / 1.5";
      break;

    default:
      printk("%s: invalid 'bitstop' value : %d\n", MODULE_NAME, bitstop);
      return (-1);
    }

  switch ((SERIAL_SIZE_DATA)(datasize))
    {
    case SIZE_5:
      s_datasize = "5";
      break;

    case SIZE_6:
      s_datasize = "6";
      break;

    case SIZE_7:
      s_datasize = "7";
      break;

    case SIZE_8:
      s_datasize = "8";
      break;

    default:
      printk("%s: invalid 'datasize' value : %d\n", MODULE_NAME, datasize);
      return (-1);
    }

  printk("%s: Vitesse, %s bauds par secondes\n", MODULE_NAME, s_speed);
  printk("%s: Parite, %s\n", MODULE_NAME, s_parite);
  printk("%s: Bit de stop, %s bit(s)\n", MODULE_NAME, s_bitstop);
  printk("%s: Taille Donnees, %s octets\n", MODULE_NAME, s_datasize);
  return (0);
}
