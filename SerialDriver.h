#pragma once

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexandre Frizac");
MODULE_DESCRIPTION("A serial driver");

#define MODULE_NAME	"SerialDriver"
#define UART_BASE	0x03F8 // UART's base I/O port-address
#define UART_IRQ	4

enum UART_INFO
  {
    UART_BUFFER		= UART_BASE + 0, // Tx | Rx | DLATCH_LOW
    UART_ITR_ENABLE	= UART_BASE + 1, // Activation des interruptions | DLATCH_HIGH
    UART_ITR_REGISTER	= UART_BASE + 2, // Read-only : Registre d'indentifications des interruptions
    UART_FIFO_CONTROL	= UART_BASE + 2, // Write-only : 
    UART_LINE_CONTROL	= UART_BASE + 3,
    UART_MODEM_CONTROL	= UART_BASE + 4,
    UART_LINE_STATUS	= UART_BASE + 5, // Read-only
    UART_MODEM_STATUS	= UART_BASE + 6,
    UART_DL_LS		= UART_BASE + 0,
    UART_DL_MS		= UART_BASE + 1
  };


/*
  La vitesse est controlé par UART_DL_LS && UART_DL_MS quand le bit 7 du LINE_CONTROL est à 1

  Informations sur les registres. (Source : http://www.ti.com/lit/ds/symlink/pc16550d.pdf)

  -- RECEIVE BUFFER / TRANSMITTER HOLDING [+0]
  Bit 0-7	: Contient le charactère en attente de lecture OU écriture

  -- INTERRUPT ENABLE REGISTER [+1]
  Bit 0		: This bit enables the Received Data Available Interrupt (and timeout interrupts in the FIFO mode)
 		  when set to logic 1
  Bit 1		: This bit enables the Transmitter Holding Register Empty Interrupt when set to logic 1.
  Bit 2		: This bit enables the Receiver Line Status Interrupt when set to logic 1.
  Bit 3		: This bit enables the MODEM Status Interrupt when set to logic 1.
  Bit 4-7	: Always 0

  -- INTERRUPT IDENTIFICATION REGISTER [+2] / Read-only
  Bit 0		: 0 => Une interruption attend
		  1 => Aucune interruption n'est en attente
  Bit 1-2	: Indentifie l'interruption avec la priorité la plus élevée en attente. (cf ADD:INTERRUPTION)
  Bit 3		: In the 16450 Mode this bit is 0. In the FIFO mode this bit is set along with bit 2 when 
		  a timeout interrupt is pending.
  Bit 4-5	: Always 0.
  Bit 6-7	: These two bits are set when FCR0=1

  -- FIFO CONTROL REGISTER [+2] / Write-only
  Bit 0		: Writing a 1 to FCR0 enables both the XMIT and RCVR FIFOs. Resetting FCR0 will clear all 
		  bytes in both FIFOs. When changing from the FIFO Mode to the 16450 Mode and vice versa, data
		  is automatically cleared from the FIFOs. This bit must be a 1 when other FCR bits are written 
		  to or they will not be programmed.
  Bit 1		: Writing a 1 to FCR1 clears all bytes in the RCVR FIFO and resets its counter logic to 0. The
		  shift register is not cleared. The 1 that is written to this bit position is self-clearing.
  Bit 2		: Writing a 1 to FCR2 clears all bytes in the XMIT FIFO and resets its counter logic to 0. The
		  shift register is not cleared. The 1 that is written to this bit position is self-clearing.
  Bit 3		: Setting FCR3 to a 1 will cause the RXRDY and TXRDY pins to change from mode 0 to mode 1 if 
		  FCR0 = 1 (see description of RXRDY and TXRDY pins)
  Bit 4-5	: FCR4 to FCR5 are reserved for future use.
  Bit 6-7	: FCR6 and FCR7 are used to set the trigger level for the RCVR FIFO interrupt.
  
  -- LINE CONTROL REGISTER [+3]
  Bit 0-1	: Longueur des charactères
  Bit 2		: Nombre de bit de stop
  Bit 3		: Parity enable
  Bit 4		: Event parity enable
  Bit 5		: Stick parity enable
  Bit 6		: Break control bit
  Bit 7		: Divisor Latch Access, 1 to access the Divisor Latches of the Baud Generator during 
		  a Read or Write operation. It must be set low (logic 0) to access the Receiver Buffer, 
		  the Transmitter Holding Register, or the Interrupt Enable Register.

  -- MODEM CONTROL REGISTER [+4]
  Bit 0		: This bit controls the Data Terminal Ready (DTR) output. When bit 0 is set to a logic 1, 
		  the DTR output is forced to a logic 0. When bit 0 is reset to a logic 0, the DTR output is 
		  forced to a logic 1.
  Bit 1		: This bit controls the Request to Send (RTS) output. When bit 1 is set to a logic 1, 
		  the RTS output is forced to a logic 0. When bit 1 is reset to a logic 0, the RTS output is 
		  forced to a logic 1.
  Bit 2		: This bit controls the Output 1 (OUT 1) signal, which is an auxiliary user-designated output. 
		  Bit 2 affects the OUT 1 output in a manner identical to that described above for bit 0.
  Bit 3		: This bit controls the Output 2 (OUT 2) signal, which is an auxiliary user-designated output. 
		  Bit 3 affects the OUT 2 output in a manner identical to that described above for bit 0.
  Bit 4		: Doit être à 0. Sert à tester l'uart
  Bit 5-7	: Always 0.

  -- LINE STATUS REGISTER [+5]
  Bit 0		: Data ready indicator quand il vaut 1. Il est mis à 0 quand on lit toutes les 
		  "Receiver Buffer Register or the FIFO"
  Bit 1		: Overrun Error (OE) indicator. Indicates that data in the Receiver Buffer Register was not read 
		  by the CPU before the next character was transferred into the Receiver Buffer Register.
  Bit 2		: Parity Error (PE)
  Bit 3		: Framing Error (FE) indicator. Indicates that the received character did not have a valid Stop bit.
  Bit 4		: Break interrupt indicator.
  Bit 5		: Transmitter Holding Register Empty (THRE) indicator.  The THRE bit is set to a logic 1 when 
		  a character is transferred from the Transmitter Holding Register into the Transmitter Shift 
		  Register. The bit is reset to logic 0 concurrently with the loading of the Transmitter Holding 
		  Register by the CPU.
  Bit 6		: Transmitter Empty (TEMT) indicator. Set to a logic 1 whenever the Transmitter Holding 
		  Register (THR) and the Transmitter Shift Register (TSR) are both empty.
  Bit 7		: In the 16450 Mode this is a 0. In the FIFO mode LSR7 is set when there is at least 
		  one parity error, framing error or break indication in the FIFO.

  -- MODEM STATUS REGISTER [+6]
  Bit 0		: This bit is the Delta Clear to Send (DCTS) indicator. Bit 0 indicates that the CTS input 
		  to the chip has changed state since the last time it was read by the CPU.
  Bit 1		: This bit is the Delta Data Set Ready (DDSR) indicator. Bit 1 indicates that the DSR input 
		  to the chip has changed state since the last time it was read by the CPU.
  Bit 2		: This bit is the Trailing Edge of Ring Indicator (TERI) detector. Bit 2 indicates that the 
		  RI input to the chip has changed from a low to a high state
  Bit 3		: This bit is the Delta Data Carrier Detect (DDCD) indicator. Bit 3 indicates that the DCD 
		  input to the chip has changed state.
  Bit 4		: This bit is the complement of the Clear to Send (CTS) input. If bit 4 (loop) of the MCR is 
		  set to a 1, this bit is equivalent to RTS in the MCR.
  Bit 5		: This bit is the complement of the Data Set Ready (DSR) input. If bit 4 of the MCR is 
		  set to a 1, this bit is equivalent to DTR in the MCR
  Bit 6		: This bit is the complement of the Ring Indicator (RI) input. If bit 4 of the MCR is 
		  set to a 1, this bit is equivalent to OUT 1 in the MCR.
  Bit 7		: This bit is the complement of the Data Carrier Detect (DCD) input. If bit 4 of the MCR is
		  set to a 1, this bit is equivalent to OUT 2 in the MCR.


 -- ADD:INTERRUPTION
 Où la description des différentes interruptions
 Valeur : Description				: Remise à zero
000|0	: Modem status change			: MSR read		 (Modem status)
001|2	: Transmitter holding register empty	: IIR read or THR write	 (Interrupt identification or transmitter)
010|4	: Received data available		: RBR read		 (Receive buffer)
011|6	: Line status change			: LSR read		 (Line status)
110|12	: Character timeout			: RBR read		 (Receive buffer)
*/

typedef enum
  {
    VIT_300 = 0,
    VIT_1200 = 1,
    VIT_2400 = 2,
    VIT_4800 = 3,
    VIT_9600 = 4,
    VIT_19200 = 5,
    VIT_38400 = 6,
    VIT_57600 = 7,
    VIT_115200 = 8
  } SERIAL_SPEED;

typedef enum 
  {
    PARITE_NONE = 0x0,
    PARITE_IMPAIRE = 0x1,
    PARITE_NONE_2 = 0x2,
    PARITE_PAIRE = 0x3
  } SERIAL_PARITE;

typedef enum 
  {
    STOP_1 = 0x0,
    STOP_2 = 0x1, // Si taille données > 5
    STOP_1HALF = 0x1 // Si taille données == 5 ; Même qu'au dessus
  } SERIAL_BIT_STOP;

typedef enum 
  {
    SIZE_5 = 0x0,
    SIZE_6 = 0x1,
    SIZE_7 = 0x2,
    SIZE_8 = 0x3
  } SERIAL_SIZE_DATA;


/* Déclaration de fonctions */
// SetSerialSpeed écrase la configure dans UART_LINE_CONTROL.
int SetSerialSpeed(SERIAL_SPEED speed); // Vitesse de transmission / reception (voir SERIAL_VITESSE_TRANSMISSION)
int CheckConfig(void);
int ShowConfig(void);

/* Macro */
#define UART_DATA_READY_INDICATOR	(inb(UART_LINE_STATUS) & 0x01)
#define UART_TRANSMITTER_HOLDING_EMPTY	(inb(UART_LINE_STATUS) & 0x20)
#define UART_CLEAR_TO_SEND		(inb(UART_MODEM_STATUS) & 0x20)
