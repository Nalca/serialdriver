#!/bin/bash

# Voir le .h pour les valeurs
SERIAL_SPEED=4
SERIAL_PARITE=0
SERIAL_BITSTOP=0
SERIAL_DATASIZE=3


if [[ "$EUID" = "0" ]]; then
    
    echo "insmod SerialDriver.ko speed=${SERIAL_SPEED} parite=${SERIAL_PARITE} bitstop=${SERIAL_BITSTOP} datasize=${SERIAL_DATASIZE}"
    insmod SerialDriver.ko speed=${SERIAL_SPEED} parite=${SERIAL_PARITE} bitstop=${SERIAL_BITSTOP} datasize=${SERIAL_DATASIZE}
    if [[ "$?" -ne "0" ]]; then
	echo "Error while using insmod, exiting ..."
	exit
    else
	SERIAL_VALUE=`dmesg -t | grep "SerialDriver:" | grep "register_chrdev successfull" | tail -n 1 | tr -dc '[0-9]\n'`
	SERIAL_VALUE__CHARACTER_COUNT=`echo "$ŜERIAL_VALUE" | wc -c`
	
	if [[ "$SERIAL_VALUE__CHARACTER_COUNT" -eq "0" ]]; then
	    echo "Unknown error (Serial value character count)"
	    exit
	elif [[ -c "/dev/serialdrv" ]]; then
	    echo "/dev/serialdrv already exist"
	else
	    echo "mknod /dev/serialdrv c ${SERIAL_VALUE} 0"
	    mknod /dev/serialdrv c ${SERIAL_VALUE} 0
	    chmod a+rw /dev/serialdrv
	fi
    fi
else
    echo "Must be root"
fi