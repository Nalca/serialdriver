#!/bin/bash

if [[ "$EUID" = "0" ]]; then
    rmmod SerialDriver.ko
    rm -f /dev/serialdrv
else
    echo "Muse be root"
fi
